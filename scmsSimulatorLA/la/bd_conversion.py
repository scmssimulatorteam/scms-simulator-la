from .models import PublicKey
from .models import PrivateKey
from .models import KeyPair
from .models import OwnerPublicKey
from .models import LaIdentification
from .models import LinkageData
from .models import PreLinkageValue
from pyrelic.relic_wrapper import BigNumber
from pyrelic.relic_wrapper import Point


def clean_string(list_of_elements):
    return str(list_of_elements).replace(" ", "")


def save_own_keys(private_key: BigNumber, public_key: Point):
    pk_bin_value = clean_string(private_key.write_bin())
    cert_pk = PrivateKey.objects.create(bin_value=pk_bin_value)

    pu_bin_value = clean_string(public_key.write_bin())
    cert_pu = PublicKey.objects.create(bin_value=pu_bin_value)

    OwnerPublicKey.objects.create(belong_to='L', public_key=cert_pu)
    keypair = KeyPair.objects.create(private_key=cert_pk, public_key=cert_pu)

    return keypair


def get_RA_public_key():
    owner_ra = OwnerPublicKey.objects.filter(belong_to='R')
    if not owner_ra:
        return None

    chave_publ_RA = Point()

    public_list = owner_ra[0].public_key.bin_value.strip('[]').split(",")
    public_bin = [int(i) for i in public_list]

    chave_publ_RA.read_bin(public_bin)
    return chave_publ_RA


def get_PCA_public_key():
    owner_pca = OwnerPublicKey.objects.filter(belong_to='P')
    if not owner_pca:
        return None

    chave_publ_PCA = Point()

    public_list = owner_pca[0].public_key.bin_value.strip('[]').split(",")
    public_bin = [int(i) for i in public_list]

    chave_publ_PCA.read_bin(public_bin)
    return chave_publ_PCA


def get_LA_keypair_values():
    has_key = OwnerPublicKey.objects.filter(belong_to='L')
    if(not has_key):
        return None, None
    else:
        keypair = KeyPair.objects.filter(public_key=has_key[0].public_key)[0]

        public_list = keypair.public_key.bin_value.strip('[]').split(",")
        public_bin = [int(i) for i in public_list]

        private_list = keypair.private_key.bin_value.strip('[]').split(",")
        private_bin = [int(i) for i in private_list]

        return private_bin, public_bin


def get_LA_id():
    la_list = LaIdentification.objects.all()
    if(not la_list):
        return None
    return la_list[0].la_id


def save_LA_id(la_id):
    la_id_saved = LaIdentification.objects.create(la_id=la_id)
    return la_id_saved


def save_linkage_data(linkage_seed, request_id,
            period_i, j_certificates):
    seed = clean_string(list(linkage_seed))
    request_id_value = clean_string(request_id)

    linkage_data = LinkageData.objects.create(linkage_seed=seed,
                                              request_id=request_id_value,
                                              period_i=period_i,
                                              j_certificates=j_certificates)
    return linkage_data


def add_pre_linkage_value(linkage_data, pre_linkage):
    pre_linkage_value = clean_string(pre_linkage)
    plv = PreLinkageValue.objects.create(value=pre_linkage_value)

    linkage_data.pre_linkage_values.add(plv)
