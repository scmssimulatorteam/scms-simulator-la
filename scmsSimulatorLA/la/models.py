from django.db import models
from pyrelic.relic_constants import BIN_BN_SIZE
from pyrelic.relic_constants import BIN_EC_SIZE
from pyrelic.relic_constants import LINKAGE_SEED_SIZE
from pyrelic.relic_constants import LINKAGE_SIZE
from django.core.validators import int_list_validator


class PrivateKey(models.Model):
    bin_value = models.CharField(max_length=BIN_BN_SIZE, validators=[int_list_validator])


class PublicKey(models.Model):
    bin_value = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])


class OwnerPublicKey(models.Model):
    """
        LA só terá:
        - sua própria chave pública (LA)
        - chave pública da RA
        - chave pública da PCA
    """
    PROFILES = (
        ('P', 'PCA'),
        ('R', 'RA'),
        ('L', 'LA')
    )

    belong_to = models.CharField(max_length=1, choices=PROFILES)
    public_key = models.ForeignKey(PublicKey, on_delete=models.CASCADE)


class KeyPair(models.Model):
    public_key = models.ForeignKey(PublicKey, on_delete=models.CASCADE)
    private_key = models.ForeignKey(PrivateKey, on_delete=models.CASCADE)


class LaIdentification(models.Model):
    la_id = models.BinaryField(max_length=4)


class PreLinkageValue(models.Model):
    value = models.CharField(max_length=LINKAGE_SIZE, validators=[int_list_validator])


class LinkageData(models.Model):
    linkage_seed = models.CharField(max_length=LINKAGE_SEED_SIZE, validators=[int_list_validator])
    request_id = models.CharField(max_length=256)  # hash size
    period_i = models.PositiveIntegerField()
    j_certificates = models.PositiveIntegerField()
    pre_linkage_values = models.ManyToManyField(PreLinkageValue)
