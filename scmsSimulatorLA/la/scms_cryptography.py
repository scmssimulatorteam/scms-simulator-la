from django.db import transaction
from pyrelic.relic_wrapper import BigNumber
from pyrelic.relic_wrapper import Point
from pyrelic.relic_wrapper import CryptoProtocols
from pyrelic.relic_wrapper import Hash
from . import bd_conversion
import os


@transaction.atomic
def generate_prelinkage_value(ra_request):
    """
        TERCEIRO MODO DE OPERAÇÃO

        =============================
        Implementação:
        =============================

        Recebe mensagem com:
            - 'recovery_point': ponto para decifrar o texto recebido,
            - 'cyphertext': valores cifrados do certificado.

        Ao decifrar com sua chave privada, a LA recebe:
            - 'request_id': Código de identificação da requisição;
            - 'period_i': inteiro representando o período para a
            solicitação do pseudônimo (i);
            - 'j_certificates': inteiro representando quantos
            certificados são válidos no mesmo período de tempo (j);

        Calcula o valor dos pre-linkage values, que são cifrados
        individualmente para a PCA:
        {
            'request_id',
            'period_i',
            'pre_linkage_values': [
                {
                    'cyphertext',  # Correspondentes a pre_linkage_value_1
                    'recovery_point'
                }, {
                    'cyphertext',  # Correspondentes a pre_linkage_value_2
                    'recovery_point'
                }, {
                    'cyphertext',  # Correspondentes a pre_linkage_value_3
                    'recovery_point'
                },
                ...,
                {
                    'cyphertext',  # Correspondentes a pre_linkage_value_j
                    'recovery_point'
                }
            ]
        }

        Esta mensagem é cifrada para a RA, retornando apenas
        {
            'cyphertext',
            'recovery_point'
        }

        =============================
        Geração de pre-linkage values
        =============================

        Seja la_id_x uma string de 32 bits associada
        à entidade LA_x.
        A LA seleciona uma string aleatória de 128 bits,
        chamada de linkage_seed inicial.

        A linkage_seed de cada semana é calculada:
            linkage_seed(i) = Hash_16(la_id || linkage_seed(i-1)),
        no qual Hash_16 significa os 16 bytes mais significativos
        obtidos com a função de hash (SHA-256).

        Para cada certificado j pertencente ao mesmo período i,
        o valor de pre-linkage é calculado:
            plv(i, j) = E_8(linkage_seed(i), la_id || j)
        no qual E_8(k, m) são os 8 bytes mais significativos
        da cifração da mensagem m com algoritmo AES-128,
        utilizando a chave k.

        Os pre-linkage values são encriptados individualmente
        com a chave pública da PCA e enviados para a RA (pacote
        todo cifrado com a chave da RA).
    """
    private, public = get_own_keys()

    bin_value = ra_request.get('recovery_point')
    recovery_point = Point()
    recovery_point.read_bin(bin_value)

    cyphertext = ra_request.get('cyphertext')

    decrypted_message = CryptoProtocols.ecies_decrypt(recovery_point, cyphertext, private)
    # print("decrypted_message: %s" % decrypted_message)

    request_id = decrypted_message.get('request_id')
    period_i = decrypted_message.get('period_i')
    j_certificates = decrypted_message.get('j_certificates')

    # os.urandom: Return a string of n random bytes suitable for cryptographic use.
    initial_linkage_seed = bytearray(os.urandom(16))
    # print("initial_linkage_seed = %s, size = %s" % (initial_linkage_seed, len(initial_linkage_seed)))

    linkage_data = bd_conversion.save_linkage_data(initial_linkage_seed, request_id, period_i, j_certificates)

    linkage_seed = []
    linkage_seed.append(initial_linkage_seed[0:16])

    la_id = get_or_generate_LA_id()
    # print("La_id = %s, size = %s" % (la_id, len(la_id)))
    for i in range(1, period_i + 1):
        # linkage_seed(i) = Hash_16(la_id || linkage_seed(i-1)),
        value = la_id + linkage_seed[i - 1]
        # print("Value: %s" % value)
        linkage_seed_i = Hash.get_hash(value)
        linkage_bytes = bytearray(linkage_seed_i, "utf-8")
        # print("Linkage_bytes= %s" % linkage_bytes)
        linkage_seed.append(linkage_bytes[0:16])

    # print("Linkage_seed: %s" % linkage_seed)
    pca_key = bd_conversion.get_PCA_public_key()
    pre_linkage_values = []

    # plv(i, j) = E_8(linkage_seed(i), la_id || j)
    for j in range(1, j_certificates + 1):
        key = linkage_seed[period_i]
        plain_text = list(la_id + bytes(j))
        iv_j = os.urandom(16)
        # print("Key: %s, plain: %s, iv: %s" % (key, plain_text, iv_j))
        pre_linkage_value_full = CryptoProtocols.aes_cbc_encrypt(plain_text, key, iv_j)

        pre_linkage_value_j = pre_linkage_value_full[0:8]
        # print("Prelinkage value: %s" % pre_linkage_value_j)

        bd_conversion.add_pre_linkage_value(linkage_data, pre_linkage_value_j)

        cyphertext_pca, recovery_point_pca = CryptoProtocols.ecies_encrypt(pca_key, pre_linkage_value_j)
        pre_linkage_values.append({
            'cyphertext': cyphertext_pca,
            'recovery_point': recovery_point_pca.write_bin()
        })

        # TODO: Salvar no BD
    # print("pre_linkage_values_cypher: %s" % pre_linkage_values)
    plain_message_ra = {
        'request_id': request_id,
        'period_i': period_i,
        'pre_linkage_values': pre_linkage_values
    }

    ra_key = bd_conversion.get_RA_public_key()
    cyphertext_ra, recovery_point_ra = CryptoProtocols.ecies_encrypt(ra_key, plain_message_ra)

    return_message = {
        'cyphertext': cyphertext_ra,
        'recovery_point': recovery_point_ra.write_bin()
    }
    return return_message


def get_own_keys():
    """
    Retorna a chave pública (Point) e privada (BigNumber) da LA
    """
    private_bin, public_bin = bd_conversion.get_LA_keypair_values()
    private = BigNumber()
    public = Point()

    if private_bin and public_bin:
        # print("TEM CHAVE!")
        private.read_bin(private_bin)
        public.read_bin(public_bin)
    else:
        # ERRO
        # print("Criando chaves!")
        CryptoProtocols.generate_keys(private, public)
        bd_conversion.save_own_keys(private, public)

    return private, public


def get_or_generate_LA_id():
    LA_id = bd_conversion.get_LA_id()
    if(LA_id):
        return bytearray(LA_id)
    else:
        new_LA_id = bytearray(os.urandom(4))
        bd_conversion.save_LA_id(new_LA_id)
        return new_LA_id
