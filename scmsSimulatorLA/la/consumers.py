# chat/consumers.py
from channels.generic.websocket import WebsocketConsumer
from la import scms_cryptography
import json


class LaConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        # print("Mensagem recebida: %s" % text_data_json)
        return_message = scms_cryptography.generate_prelinkage_value(text_data_json)
        # print("Enviando: %s" % return_message)
        self.send(text_data=json.dumps({
            'message': return_message
        }))
