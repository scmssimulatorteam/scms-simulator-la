from django.apps import AppConfig


class LaConfig(AppConfig):
    name = 'la'

    def ready(self):
        from pyrelic import relic_config
        relic_config.setup_relic()
