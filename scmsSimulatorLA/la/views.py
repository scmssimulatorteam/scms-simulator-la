from django.shortcuts import render
from . import scms_cryptography
from .models import LinkageData


def index(request):
    la_id = list(scms_cryptography.get_or_generate_LA_id())

    linkage_data = LinkageData.objects.all().order_by('-id')

    context = {
        'linkage_data': linkage_data,
        'la_id': la_id
    }
    return render(request, 'la/index_mode_3.html', context)
