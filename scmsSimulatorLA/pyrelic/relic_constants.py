# Constantes

WORD = 64
DIGIT = WORD
DIGIT_LOG = 6

STS_OK = 0
STS_ERR = 1

CMP_EQ = 0
CMP_NE = 2

BN_PRECI = 1024
BN_BITS = BN_PRECI
BN_DIGIT = DIGIT
BN_DIG_LOG = DIGIT_LOG
BN_DIGS = ((BN_BITS) / (BN_DIGIT) + (BN_BITS % BN_DIGIT > 0))
BN_BYTES = ((BN_BITS) / 8 + ((BN_BITS % 8) > 0))
BN_SIZE = (2 * BN_DIGS + 2)
BN_POS = 0
BN_NEG = 1

MD_LEN_SH256 = 32
MD_EXIT_HEX_DIGITS = 64

FP_PRIME = 254
FP_DIGS = (int(FP_PRIME / WORD) + 1)

ec_compress = 1  # the flag to indicate compression

BIN_BN_SIZE = 32 * 5  # 32 itens, cada item podendo ser representado por até 5 caracteres (3 dígitos + vírgula) + 2 colchetes
BIN_EC_SIZE = 33 * 5

LINKAGE_SEED_SIZE = 16 * 4 + 2  # (16 itens de até 3 dígitos + vírgula) + dois colchetes
LINKAGE_SIZE  = 8 * 4 + 2  # (8 itens de até 3 dígitos + vírgula) + dois colchetes
