/*
 * RELIC is an Efficient LIbrary for Cryptography
 * Copyright (C) 2007-2015 RELIC Authors
 *
 * This file is part of RELIC. RELIC is legal property of its developers,
 * whose names are not listed here. Please refer to the COPYRIGHT file
 * for contact information.
 *
 * RELIC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * RELIC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RELIC. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 *
 * Implementação das macros de EC
 *
 * @ingroup bn
 */

#include <errno.h>
#include "relic_core.h"
#include "relic_ec.h"

/*============================================================================*/
/* Public definitions                                                         */
/*============================================================================*/

/*============================================================================*/
/* relic_ec.h 		                                                          */
/*============================================================================*/


void ec_null_macro(ec_t A) {
	ep_null(A);
}

void ec_new_macro(ec_t A) {
	ep_new(A);
}

int ec_param_set_any_macro() {
	return ep_param_set_any();
}

void ec_param_print_macro() {
	ep_param_print();
}

int ec_cmp_macro(const ec_t P, const ec_t Q) {
	return ep_cmp(P, Q);
}

void ec_rand_macro(ec_t P) {
	ep_rand(P);
}

int ec_is_valid_macro(ec_t P) {
	return ep_is_valid(P);
}

void ec_print_macro(ec_t P) {
	ep_print(P);
}

void ec_add_macro(ec_t R, const ec_t P, const ec_t Q) {
	ep_add(R, P, Q);
}

void ec_sub_macro(ec_t R, const ec_t P, const ec_t Q) {
	ep_sub(R, P, Q);
}

void ec_mul_macro(ec_t R, const ec_t P, const bn_t K) {
	ep_mul(R, P, K);
}

void ec_mul_gen_macro(ec_t R, const bn_t K) {
	ep_mul_gen(R, K);
}

void ec_mul_sim_macro(ec_t R, const ec_t P, const bn_t K, const ec_t Q,
		const bn_t M) {
	ep_mul_sim(R, P, K, Q, M);
}

void ec_mul_sim_gen_macro(ec_t R, const bn_t K, const ec_t Q, const bn_t M) {
	ep_mul_sim_gen(R, K, Q, M);
}

void ec_curve_get_ord_macro(bn_t n) {
	ep_curve_get_ord(n);
}


void ec_read_bin_macro(ec_t A, const uint8_t *B, int L) {
	ep_read_bin(A, B, L);
}	

int ec_size_bin_macro(ec_t A, int P) {
	return ep_size_bin(A, P);
}

void ec_write_bin_macro(uint8_t *B, int L, ec_t A, int P) {
	ep_write_bin(B, L, A, P);
}
	
int ec_param_level_macro() {
	return ep_param_level();
}

void ec_norm_macro(ec_t r, const ec_t p) {
	ep_norm(r, p);
}