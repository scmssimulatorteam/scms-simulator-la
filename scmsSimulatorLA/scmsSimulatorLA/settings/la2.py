from .base import *

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'scmssimulatorla_2',
        'USER': 'scmsuser',
        'PASSWORD': 'scmsprojectpassword',
        'HOST': 'localhost',
        'PORT': '',
    },
    # 'default': {
    #     'ENGINE': 'django.db.backends.sqlite3',
    #     'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    # }
}

# URLs
SOCKET_HOST_NAME = '127.0.0.1'
SOCKET_HOST_URL = 'ra/'
PORT = 9000
