# Simulador SCMS PCA

### Pré-requisitos
- Python 3.7.0
- pip 10.0.1
- virtualenv 16.0.0
- PosgreSQL 10.4


## Configuração do banco de dados

- Criar uma database:
```
CREATE DATABASE scmssimulatorla;
CREATE DATABASE scmssimulatorla_2;
```

- Criar o usuário e alterar suas configurações
```                                                                                          
create user scmsuser with password 'scmsprojectpassword';
ALTER ROLE scmsuser SET client_encoding TO 'utf8';
ALTER ROLE scmsuser SET default_transaction_isolation TO 'read committed';
GRANT ALL PRIVILEGES ON DATABASE scmssimulatorla TO scmsuser;
GRANT ALL PRIVILEGES ON DATABASE scmssimulatorla_2 TO scmsuser;
ALTER ROLE scmsuser SET timezone TO 'UTC';
```

## Instalação

- Criar um ambiente virtual
```
virtualenv env --python=python3.6 
```

- Acessar o ambiente virtual
```
source env/bin/activate
```

- Compilar a biblioteca e instalar requisitos através do Makefile
```
make all
```

- Acessar a pasta do projeto que deseja rodar. Exemplo: scmsSimulatorPCA.
```
cd scmsSimulatorLA
```

- Gerar e aplicar migrações nos bancos de dados:
```
python manage.py makemigrations --settings=scmsSimulatorLA.settings.la1
python manage.py migrate --settings=scmsSimulatorLA.settings.la1

python manage.py makemigrations --settings=scmsSimulatorLA.settings.la2
python manage.py migrate --settings=scmsSimulatorLA.settings.la2
```

- Executar scripts no banco de dados:
```
\connect scmssimulatorla


-- Adiciona própria chave

INSERT INTO public.la_publickey(bin_value)
    VALUES ('[3,94,22,139,172,56,30,205,186,159,45,156,6,37,239,186,71,26,128,37,197,177,20,60,155,120,53,191,75,241,157,47,175]');

INSERT INTO public.la_privatekey(bin_value)
	VALUES ('[15,173,158,197,42,12,156,64,95,157,94,52,177,151,24,193,191,127,104,176,117,153,4,234,6,4,133,163,199,198,222,63]');

INSERT INTO public.la_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('L', (SELECT id from public.la_publickey WHERE bin_value='[3,94,22,139,172,56,30,205,186,159,45,156,6,37,239,186,71,26,128,37,197,177,20,60,155,120,53,191,75,241,157,47,175]'));

INSERT INTO public.la_keypair(private_key_id, public_key_id)
	VALUES (
		(SELECT id from public.la_privatekey WHERE bin_value='[15,173,158,197,42,12,156,64,95,157,94,52,177,151,24,193,191,127,104,176,117,153,4,234,6,4,133,163,199,198,222,63]'),
		(SELECT id from public.la_publickey WHERE bin_value='[3,94,22,139,172,56,30,205,186,159,45,156,6,37,239,186,71,26,128,37,197,177,20,60,155,120,53,191,75,241,157,47,175]')
		);


-- Adiciona a chave da RA
INSERT INTO public.la_publickey(bin_value)
    VALUES ('[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]');

INSERT INTO public.la_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('R', (SELECT id from public.la_publickey WHERE bin_value='[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]' LIMIT 1));


-- Adiciona chave da PCA
INSERT INTO public.la_publickey(bin_value)
    VALUES ('[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]');

INSERT INTO public.la_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('P', (SELECT id from public.la_publickey WHERE bin_value='[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]' LIMIT 1));



\connect scmssimulatorla_2

-- Adiciona própria chave

INSERT INTO public.la_publickey(bin_value)
    VALUES ('[3,54,8,69,56,231,210,116,125,115,251,29,38,2,186,17,72,12,194,176,182,107,180,161,13,149,89,252,235,133,150,99,228]');

INSERT INTO public.la_privatekey(bin_value)
	VALUES ('[1,236,38,233,53,124,130,223,35,176,18,123,109,50,180,174,128,205,227,29,132,181,198,127,7,37,122,220,184,202,203,241]');

INSERT INTO public.la_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('L', (SELECT id from public.la_publickey WHERE bin_value='[3,54,8,69,56,231,210,116,125,115,251,29,38,2,186,17,72,12,194,176,182,107,180,161,13,149,89,252,235,133,150,99,228]'));

INSERT INTO public.la_keypair(private_key_id, public_key_id)
	VALUES (
		(SELECT id from public.la_privatekey WHERE bin_value='[1,236,38,233,53,124,130,223,35,176,18,123,109,50,180,174,128,205,227,29,132,181,198,127,7,37,122,220,184,202,203,241]'),
		(SELECT id from public.la_publickey WHERE bin_value='[3,54,8,69,56,231,210,116,125,115,251,29,38,2,186,17,72,12,194,176,182,107,180,161,13,149,89,252,235,133,150,99,228]')
		);


-- Adiciona a chave da RA
INSERT INTO public.la_publickey(bin_value)
    VALUES ('[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]');

INSERT INTO public.la_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('R', (SELECT id from public.la_publickey WHERE bin_value='[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]' LIMIT 1));


-- Adiciona chave da PCA
INSERT INTO public.la_publickey(bin_value)
    VALUES ('[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]');

INSERT INTO public.la_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('P', (SELECT id from public.la_publickey WHERE bin_value='[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]' LIMIT 1));
```

- Executar o programa
```
python manage.py runserver 5000 --settings=scmsSimulatorLA.settings.la1
```
OU
```
python manage.py runserver 5001 --settings=scmsSimulatorLA.settings.la2
```


## Observações

- Se for necessário compilar apenas a biblioteca novamente, basta executar o comando
```
make relic
```

- Login admin
```
user: giuliana
senha: scmsadmin
```